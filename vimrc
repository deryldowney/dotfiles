set nocompatible " Be iMproved
filetype off

" Vundle
set rtp+=~/.vim/bundle/vundle
call vundle#begin()
Plugin 'gmarik/vundle'


 " Brief help
 " :BundleList          - list configured bundles
 " :BundleInstall(!)    - install(update) bundles
 " :BundleSearch(!) foo - search(or refresh cache first) for foo
 " :BundleClean(!)      - confirm(or auto-approve) removal of unused bundles
 "
 " see :h vundle for more details or wiki for FAQ
 " NOTE: comments after Bundle command are not allowed..

Plugin 'guicolorscheme.vim'
Plugin 'YankRing.vim'
Plugin 'Raimondi/delimitMate'
Plugin 'kien/ctrlp.vim'
Plugin 'tpope/vim-eunuch'
Plugin 'sudo.vim'
Plugin 'Shougo/neocomplcache.vim'
Plugin 'mileszs/ack.vim'
Plugin 'tomtom/tcomment_vim'
Plugin 'vim-ruby/vim-ruby'
Plugin 'garbas/vim-snipmate'
Plugin 'honza/vim-snippets'
Plugin 'Lokaltog/powerline'
Plugin 'scrooloose/nerdtree.git'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'altercation/vim-colors-solarized'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-endwise'
Plugin 'scrooloose/syntastic'
Plugin 'MarcWeber/vim-addon-mw-utils'
Plugin 'tomtom/tlib_vim'
Plugin 'nathanaelkane/vim-indent-guides'
Plugin 'xmisao/rubyjump.vim'
Plugin 'kana/vim-textobj-user'
Plugin 'nelstrom/vim-textobj-rubyblock'
Plugin 'tpope/vim-bundler'
Plugin 'tpope/vim-rake'
Plugin 'tpope/vim-rails'
Plugin 'thoughtbot/vim-rspec'
Plugin 'tpope/vim-dispatch'
Plugin 'tpope/vim-cucumber'
Plugin 'kchmck/vim-coffee-script'
Plugin 'junegunn/vim-github-dashboard'
Plugin 'tpope/vim-git'
Plugin 'plasticboy/vim-markdown'
Plugin 'othree/html5.vim'
Plugin 'pangloss/vim-javascript'
Plugin 'nono/jquery.vim'
Plugin 'burnettk/vim-angular'
Plugin 'matthewsimo/angular-vim-snippets'
Plugin 'guns/vim-clojure-static'
Plugin 'tpope/vim-haml'
Plugin 'groenewege/vim-less'
Plugin 'hail2u/vim-css3-syntax'
Plugin 'elzr/vim-json'
Plugin 'tpope/vim-fireplace'
Plugin 'johnsyweb/vim-makeshift'
Plugin 'mattn/webapi-vim'
Plugin 'mattn/gist-vim'
Plugin 'tfnico/vim-gradle'
Plugin 'godlygeek/csapprox'
Plugin 'sjl/gundo.vim'
Plugin 'chrisbra/unicode.vim'
Plugin 'benmills/vimux'
Plugin 'skalnik/vim-vroom'
Plugin 'pgr0ss/vimux-ruby-test'
Plugin 'cloud8421/vimux-cucumber'
Plugin 'jgdavey/vim-turbux'
Plugin 'jingweno/vimux-zeus'
Plugin 'christoomey/vim-tmux-navigator'
Plugin 'tyru/open-browser.vim'
Plugin 'vim-scripts/bufexplorer.zip'

" All of your Plugins must be added before the following line
call vundle#end()            " required

filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just
" :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" Config base

" Make vim interact with the system keyboard more easily
set clipboard=unnamed

" For vim-snipmate
let g:snipMate = {}
let g:snipMate.scope_aliases = {}
let g:snipMate.scope_aliases['ruby'] = 'ruby,ruby-rails,ruby-1.9'

" For textobj
runtime macros/matchit.vim

" For matchit.vim
if has("autocmd")
  filetype indent plugin on
endif

" This is for the Syntastic plugin, we need to set it before it loads
let g:syntastic_enable_signs = 1
let g:syntastic_echo_current_error = 1
let g:syntastic_auto_loc_list = 2
let g:syntastic_enable_highlighting = 1
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0


set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:rspec_command = "Dispatch rspec {spec}"

" Toggle input/normal mode by leader
nmap <leader>i i
imap <leader>i <Esc>l

vnoremap <c-a> :Inc<CR>
vnoremap # y?\V<C-R>=substitute(escape(@@,"/\\"),"\n","\\\\n","ge")<CR><CR>
vnoremap * y/\V<C-R>=substitute(escape(@@,"/\\"),"\n","\\\\n","ge")<CR><CR>

au BufRead,BufNewFile *.rb set tags=~/.tags,.tags,tags,ctags
au BufNewFile,BufRead *.m{,ar}{,k}d{,own} set ai formatoptions=tcroqn2 comments=n:> syntax=mkd
au BufNewFile,BufRead *.rhtml setf eruby
au BufNewFile,BufRead *.(xhtml|nag|zmr) setf nagoro
au BufNewFile,BufRead *.(byss|rake|ru) set filetype=ruby syntax=ruby
au BufNewFile,BufRead .irbrc  setf ruby
au BufNewFile,BufRead *.st    setf st
au BufNewFile,BufRead *.haml  setf haml
au BufNewFile,BufRead *.neko  setf neko
au BufNewFile,BufRead *.ox    setf oxid
au BufNewFile,BufRead *.fdi   setf xml
au BufNewFile,BufRead *.rl    setf ragel
au BufNewFile,BufRead *.sass  setf sass
au BufNewFile,BufRead *.go    setf go
au BufRead,BufNewFile *.thor set filetype=ruby

au FileType ruby,eruby,nagoro set omnifunc=rubycomplete#Complete
au FileType ruby,eruby,nagoro let g:rubycomplete_buffer_loading = 1
au FileType ruby,eruby,nagoro let g:rubycomplete_classes_in_global = 1
au FileType neko map <F5> :w<CR>:!rake run:%<CR>
au FileType oxid map <F5> :w<CR>:!oxid %<CR>
au FileType ruby map <F5> :w<CR>:!ruby %<CR>

au FileType haskell,vhdl,ada            let b:comment_leader = '-- '
au FileType vim                         let b:comment_leader = '" '
au FileType c,cpp,java,go               let b:comment_leader = '// '
au FileType sh,make,ruby                let b:comment_leader = '# '
au FileType tex                         let b:comment_leader = '% '
noremap <silent> ,c :<C-B>sil <C-E>s/^/<C-R>=escape(b:comment_leader,'\/')<CR>/<CR>:noh<CR>
noremap <silent> ,u :<C-B>sil <C-E>s/^\V<C-R>=escape(b:comment_leader,'\/')<CR>//e<CR>:noh<CR>
" ,c comments out a region
" ,u uncomments a region

" Trim whitespace from ruby files
au BufWritePre *.rb normal m`:%s/\s\+$//e ``
au BufWritePre *.rb normal m`:%s/\s\+$//e `` "   exec 'v:^--\s*$:s:\s\+$::e'

" au BufWritePre *.rb :!ruby -c %
au BufWritePost ~/.vimrc so ~/.vimrc
let g:checksyntax_auto = 1
let g:checksyntax_auto_ruby = 1

" Remove trailing blanks upon saving except from lines
" containing sigdashes: '--  '
" TODO: find a way to :w only if changes were made
function! StripTrailingSpaces()
  exe "normal :%s/\\s\\+$//g\<CR>"
endfunction
"
" au BufWritePre * silent! call StripTrailingSpaces()
au BufEnter,BufRead *.rb silent! call StripTrailingSpaces()

" configure CtrlP
let g:ctrlp_map = ''
let g:ctrlp_max_height = 45
let g:ctrlp_match_window_reversed = 0
let g:ctrlp_match_window_bottom = 1
let g:ctrlp_switch_buffer = 2
let g:ctrlp_working_path_mode = 2
let g:ctrlp_mruf_include = '\.py$\|\.rb$|\.coffee|\.haml'
let g:ctrlp_custom_ignore = '\.git$\|\.hg$\|\.svn$'
let g:ctrlp_follow_symlinks = 1
let g:ctrlp_extensions = ['tag', 'buffertag', 'quickfix', 'dir', 'rtscript']
let g:ctrlp_user_command = {
      \ 'types': {
      \ 1: ['.git/', 'cd %s && git ls-files'],
      \ 2: ['.hg/', 'hg --cwd %s locate -I .'],
      \ },
      \ 'fallback': 'find %s -type f'
      \ }

" configure IndentGuides plugin
let g:indent_guides_auto_colors = 0
let g:indent_guides_start_level = 3
let g:indent_guides_guide_size  = 1
autocmd! VimEnter * hi IndentGuidesOdd ctermbg=236 guibg=#303030 | hi IndentGuidesEven ctermbg=239 guibg=#505050
autocmd BufRead * IndentGuidesEnable
autocmd! VimEnter,Colorscheme * hi IndentGuidesEven ctermbg=123 guibg=#880055

" configure grep program to be ack. Using ack.vim definition
set grepprg=ack

" configure ack.vim plugin
let g:ackprg = 'ack --nogroup --column'

syntax on
filetype plugin on
filetype indent on
set number " Show line numbers
set nopaste

" Enable syntax folding for blocks and comments.
set foldmethod=syntax
set foldminlines=3
set foldlevel=100

" indentation configuration
set cindent
set smartindent
set autoindent

" Convert tabs to spaces, use 2 spaces in place of tabs.
set expandtab
set tabstop=2
set shiftwidth=2

" text search options
set hlsearch
set incsearch
set ignorecase
set smartcase

" hide buffers instead of closing them when you :q, keeping their undo history
set hidden

" Open new windows on the bottom and right instead of the top and left.
set splitbelow
set splitright

" increase the default command line history
set history=1000

" File name tab completion functions like bash, it gives you a list of
" options instead of automatically filling in the first possible match.
set wildmenu
" It will however, with this option, complete up to the first character of
" ambiguity.
set wildmode=list:longest

" Make clipbord work on OS X. This makes copy/paste operations trivial between
" vim and other applications since they all use the same clipboard now.
set clipboard=unnamed
" visual select automatically copies to X11's selection ("middle click") buffer
set go+=a

" scrolls the buffer before you reach the last line of the window
set scrolloff=3

" Always show status line
set laststatus=2

" default encoding
set encoding=utf-8

" sets backspace key functions, allows it to backspace over end of line
" characters, start of line, and indentation
set backspace=indent,eol,start
" movement keys will take you to the next or previous line
set whichwrap+=<,>,h,l

" enable mouse in console
set mousemodel=extend
set mouse=a
set mousehide

" improve autocomplete menu color
highlight Pmenu ctermbg=238 gui=bold

" set the spellcheck language
setlocal spell spelllang=en_us
" disable spellcheck by default
set nospell

" use a user-local vim-specific directory for backups rather than the global
" tmp directory by default
set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp

" treat question marks as part of a word in ruby
autocmd BufRead *.rb,*.rake,*.rhtml,<ruby> set isk=?,@,48-57,_,192-255

" ruby
autocmd FileType ruby,eruby set omnifunc=rubycomplete#Complete
autocmd FileType ruby,eruby let g:rubycomplete_buffer_loading = 1
autocmd FileType ruby,eruby let g:rubycomplete_rails = 1
autocmd FileType ruby,eruby let g:rubycomplete_classes_in_global = 1

" disable wordwrap when looking at CSVs
autocmd BufRead *.csv,*.csv*,<csv> set nowrap

" Set up vim for git commits to add spell checking and word wrapping
autocmd Filetype gitcommit setlocal spell textwidth=72

" remove whistespace at end of line before write
func! StripTrailingWhitespace()
  normal mZ
  %s/\s\+$//e
  normal `Z
endfunc
au BufWrite * if ! &bin | call StripTrailingWhitespace() | endif

" display the file name of the current file in the Terminal (xterm/item/&c) title
if has('title')
  set title
  autocmd BufEnter * let &titlestring = "vim: " . expand("%:p:~")
  "set titlestring=%t%(\ [%R%M]%)
  "autocmd BufEnter * exe "echo '\033'+bufname("%")+'\007'"
endif

if has('gui_running')

  " for Gui versions of vim. see :help guioptions for more info
  set guifont=Monaco\ for\ Powerline
  set guioptions=aAce
  colorscheme railscasts

  " for MacVim
  if has("macunix")
    set transparency=8
  end

elseif version >= 700 && &term != 'cygwin'

  " configure 256 color schemes for terminal using CSApprox or guicolorscheme
  set t_Co=256
  if has('gui')
    let g:CSApprox_attr_map = { 'bold' : 'bold', 'italic' : '', 'sp' : '' }
    colorscheme railscasts
  else
"    GuiColorScheme railscasts
  endif

endif

" Disable balloon popup since theres a plugin that makes it really annoying
if has("balloon_eval")
  set noballooneval
  set balloondelay=100000
end

" NerdTree
" Set default Leader
let mapleader = ','

" Enable NERDTree
map <Leader>n :NERDTreeTabsToggle<CR>
let g:NERDTreeDirArrows=0

" for rails.vim swap to model/control/etc from associated file
map <leader>rm :Rmodel<CR>
map <leader>rc :Rcontroller<CR>
map <leader>rh :Rhelper<CR>
map <leader>ru :Runittest<CR>
map <leader>rf :Rfunctionaltest<CR>
map <leader>ro :Robserver<CR>
map <leader>rv :Rview<CR>
map <leader>rl :Rlocale<CR>
" for CtrlP
map <leader>ff :CtrlP<CR>
map <leader>fb :CtrlPBuffer<CR>
map <leader>ft :CtrlPTag<CR>
map <leader>fq :CtrlPQuickFix<CR>
map <leader>fd :CtrlPDir<CR>
map <leader>fr :CtrlPRTS<CR>
map <leader>fm :CtrlPMRU<CR>
" for gundo
map <leader>g :GundoToggle<CR>
" runs diff against the current buffer and the file on disk
map <leader>d :w !diff % -<CR>
" When pressing <leader>cd switch to the directory of the open buffer
map <leader>cd :cd %:p:h<CR>
" search hilighting control, enables and disable intelligently and toggles
nnoremap / :set hlsearch<CR>/
nnoremap ? :set hlsearch<CR>?
nnoremap n :set hlsearch<CR>n
nnoremap N :set hlsearch<CR>N
nnoremap <CR> :noh<CR><CR>
nnoremap <leader>/ :set hlsearch!<CR>

" tab navigation like firefox
nmap <C-S-tab> :tabprevious<CR>
nmap <C-tab> :tabnext<CR>
map <C-S-tab> :tabprevious<CR>
map <C-tab> :tabnext<CR>
imap <C-S-tab> <ESC>:tabprevious<CR>i
imap <C-tab> <ESC>:tabnext<CR>i
nmap <C-t> :tabnew<CR>
imap <C-t> <ESC>:tabnew<CR>

" Bash like keys for the command line
cnoremap <C-A> <Home>
cnoremap <C-E> <End>
cnoremap <C-K> <C-U>

cnoremap <C-P> <Up>
cnoremap <C-N> <Down>

" Smart way to move between windows
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

" make mouse scrolling work in vim!!!
map <M-Esc>[62~ <ScrollWheelUp>
map <M-Esc>[63~ <ScrollWheelDown>
map <M-Esc>[64~ <S-ScrollWheelUp>
map <M-Esc>[65~ <S-ScrollWheelDown>
map! <M-Esc>[62~ <ScrollWheelUp>
map! <M-Esc>[63~ <ScrollWheelDown>
map! <M-Esc>[64~ <S-ScrollWheelUp>
map! <M-Esc>[65~ <S-ScrollWheelDown>

" For Gist
let g:gist_clip_command = 'xclip -selection clipboard'
let g:gist_detect_filetype = 1
let g:gist_open_browser_after_post = 1
let g:gist_show_privates = 1
let g:gist_get_multiplefile = 1

" Open tabs using Ctl+T + Direction arrow keys
map tt<up> :tabr<cr>
map tt<down> :tabl<cr>
map tt<left> :tabp<cr>
map tt<right> :tabnext<cr>
map tn :tabnew<cr>

" For Ack
map ,a :Ack

" For vimux
" Prompt for a command to run
map rp :PromptVimTmuxCommand
" Run last command executed by RunVimTmuxCommand
map rl :RunLastVimTmuxCommand
" Inspect runner pane
map ri :InspectVimTmuxRunner
" Close all other tmux panes in current window
map rx :CloseVimTmuxPanes
" Interrupt any command running in the runner pane
map rs :InterruptVimTmuxRunner

" For vim-vroom
" Run the current file as a Test file
map rt :VroomRunTestFile<cr>

