%w[rubygems pp logger jist].each do |gem|
  begin
    require gem
  rescue LoadError
  end
end

Pry.config.editor = "vim"
Pry.plugins["doc"].activate!

# Default Command Set, add custom commands here
default_command_set = Pry::CommandSet.new do
  command "sql", "Send sql over AR." do |query|
    if ENV['RAILS_ENV'] || defined?(Rails)
      pp ActiveRecord::Base.connection.select_all(query)
    else
      pp "Pry did not require the environment, try `pconsole`"
    end
  end
  command "caller_method" do |depth|
    depth = depth.to_i || 1
    if /^(.+?):(\d+)(?::in `(.*)')?/ =~ caller(depth+1).first
      file = Regexp.last_match[1]
      line = Regexp.last_match[2].to_i
      method = Regexp.last_match[3]
      output.puts [file, line, method]
    end
  end
end

Pry.config.commands.import default_command_set

rails = File.join Dir.getwd, 'config', 'environment.rb'

if File.exist?(rails) && ENV['SKIP_RAILS'].nil?
  require rails
  if Rails.version[0..0] == "2"
    require 'console_app'
    require 'console_with_helpers'
  elsif Rails.version[0..0] == "3"
    require 'rails/console/app'
    require 'rails/console/helpers'
  elsif Rails.version[0..0] == "4"
    require 'rails/console/app'
    require 'rails/console/helpers'
    ActiveRecord::Base.logger = Logger.new(STDOUT)
  else
    warn "[WARN] cannot load Rails console commands."
  end
end

#Pry.config.theme = "zenburn"
Pry.config.theme = "railscasts"
