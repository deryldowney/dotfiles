# set parameters:
# -s: server option
# -w: window option
#   : session option
# -g: global session/window option
# -a: append to string
# -u: inherit from global options

# Make the prefix match screen
unbind C-b # Default prefix
set -g prefix C-a
bind C-a send-prefix

set -g default-terminal "screen-256color"
set -g mode-mouse on

# Custom bindings
bind r source-file ~/.tmux.conf \; display "Reloaded!"

# Wait a second for delay between repeating commands
set -g repeat-time 1000
set -s escape-time 0

# Start indexing at 1 for keyboard sanity
set -g base-index 1
setw -g pane-base-index 1

# Who doesn't love pipes and dashes?
bind | split-window -h
bind - split-window -v

# Vim binding for splitting
unbind % # Default for split-window -h
unbind s # Default for interactive session selection
unbind '"' # Default for split-window -v
bind v split-window -h
bind s split-window -v

# Vim key bindings
setw -g mode-keys vi
set -g status-keys vi
bind -t vi-copy 'v' begin-selection
bind -t vi-copy 'y' copy-selection

# Vim-like pane & window selection/resizing
unbind l
unbind Up
unbind Down
unbind Left
unbind Right
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R
bind -r Left select-window -t :-
bind -r Right select-window -t :+

unbind M-Up
unbind M-Down
unbind M-Left
unbind M-Right
bind -r + resize-pane -U 5
bind -r - resize-pane -D 5
bind -r < resize-pane -L 5
bind -r > resize-pane -R 5
bind -r H resize-pane -L 5
bind -r J resize-pane -D 5
bind -r K resize-pane -U 5
bind -r L resize-pane -R 5

# Use the system clipboard
bind C-c run "tmux save-buffer - | xclip -i -sel clipboard"
bind C-v run "tmux set-buffer \"$(xclip -o -sel clipboard)\"; tmux paste-buffer"


# Pane selection
bind 1 select-window -t 1
bind 2 select-window -t 2
bind 3 select-window -t 3
bind 4 select-window -t 4
bind 5 select-window -t 5
bind 6 select-window -t 6
bind 7 select-window -t 7
bind 8 select-window -t 8
bind 9 select-window -t 9

# Mice settings, or as I like to say 'noob mode'
setw -g mode-mouse on
set -g mouse-select-pane on
set -g mouse-resize-pane off
set -g mouse-select-window on

# Colors!
set -g status-fg white
set -g status-bg black
setw -g window-status-fg cyan
setw -g window-status-bg default
setw -g window-status-attr dim

setw -g window-status-current-fg colour28
setw -g window-status-current-bg default
setw -g window-status-current-attr bright

set -g pane-border-fg black
set -g pane-border-bg default
set -g pane-active-border-fg green
set -g pane-active-border-bg default

set -g message-fg white
set -g message-bg black
set -g message-attr bright

set -g status-right '#[fg=colour245] %R | %d %b #[fg=colour254,bg=colour234,nobold]#[fg=colour16,bg=colour254,bold] #H '
set -g status-left ''

# Activity
setw -g monitor-activity on
set -g visual-activity off

# Boring stuff
set -g status-utf8 on
set -g status-interval 1
set -g default-terminal "screen-256color"
setw -g aggressive-resize on
# set -g automatic-rename
# enable wm window titles
#set -g set-titles on
# wm windows titles string
set -g set-titles-string '[#(whoami)@#h:#S:#I] #W'

setw -g window-status-format "#I: #T #F"
setw -g window-status-current-format "#I: #T#F"
bind \ confirm-before "kill-server"

# set -g default-shell /usr/local/bin/zsh
# set -g default-command "reattach-to-user-namespace -l $SHELL -l"
set -g history-limit 5000

# Add a key for opening new windows to remote hosts.
bind-key S command-prompt -p "SSH to Host:" "if-shell '[ -n \"%%\" ]' 'new-window -n \"%1\" \"/usr/bin/ssh %1\"'"
bind-key A command-prompt -p "AutoSSH to Host:" "if-shell '[ -n \"%%\"' 'new-window -n \"%1\" \"/usr/local/bin/autossh %1\"'"
# open a man page on new window
bind '?' command-prompt -p 'man?' "split-window 'exec man %%'"

# quick view of processes
bind '~' split-window "exec htop"

# bind Tab to last window
bind Tab last-window

# set clipboard
set -g set-clipboard on

set -g status-left-length 52
set -g status-right-length 451
set -g status-fg white
set -g status-bg colour234
set -g window-status-activity-attr bold
set -g pane-border-fg colour245
set -g pane-active-border-fg colour39
set -g message-fg colour16
set -g message-bg colour221
set -g message-attr bold
set -g status-left '#[fg=colour235,bg=colour252,bold] ❐ #S #[fg=colour252,bg=colour238,nobold]#[fg=colour245,bg=colour238,bold] #(whoami) #[fg=colour238,bg=colour234,nobold] '

set -g window-status-format "#[fg=white,bg=colour234] #I #W "
set -g window-status-current-format "#[fg=colour234,bg=colour39]#[fg=colour25,bg=colour39,noreverse,bold] #I  #W #[fg=colour39,bg=colour234,nobold]"

