# Language
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8
export LC_CTYPE="en_US.UTF-8"
export LC_NUMERIC="en_US.UTF-8"
export LC_TIME="en_US.UTF-8"
export LC_COLLATE="en_US.UTF-8"
export LC_MONETARY="en_US.UTF-8"
export LC_MESSAGES="en_US.UTF-8"
export LC_PAPER="en_US.UTF-8"
export LC_NAME="en_US.UTF-8"
export LC_ADDRESS="en_US.UTF-8"
export LC_TELEPHONE="en_US.UTF-8"
export LC_MEASUREMENT="en_US.UTF-8"
export LC_IDENTIFICATION="en_US.UTF-8"
export LC_ALL=en_US.UTF-8

# Set Timezone and TERM
TZ=America/Toronto
TERM='xterm-256color'
export TZ TERM

# Set vi mode in terminal
set -o vi

# Turn off XOFF (Ctl+s) since it constantly gets in my way
/bin/stty ixany
/bin/stty ixoff -ixon

# Personal man pages
export MANPATH="$HOME/.man:$MANPATH"

# Define editor
export EDITOR='vim'
export VISUAL='vim'
export PAGER='less'

## If we have a .profile then load it
#if [ -f "$HOME/.profile" ]; then
#  source ~/.profile
#fi

# This should eliminate tmux overwriting the last line of display inside it's windows
if [ -n "$TMUX" ]; then
  export TERM=screen-256color
fi

# If the home bin directory exists, add it to the PATH
if [ -d "$HOME/bin" ]; then
  PATH="$HOME/bin:$PATH"
  export PATH
fi

if [ -d "$HOME/Applications/node" ]; then
  PATH="$HOME/Applications/node/bin:$PATH"
fi

if [ -d "$HOME/Applications/idea" ]; then
  PATH="$HOME/Applications/idea/bin:$PATH"
fi

if [ -d "$HOME/Applications/RubyMine" ]; then
  PATH="$HOME/Applications/RubyMine/bin:$PATH"
fi

if [ -d "$HOME/Applications/android-studio/bin" ]; then
  PATH="$HOME/Applications/android-studio/bin:$PATH"
fi

# Now load any personal bash methods we've made.
if [ -f "$HOME/.bash_methods" ]; then
  source $HOME/.bash_methods
fi

# Now load any bash aliases.
if [ -f "$HOME/.bash_aliases" ]; then
  source $HOME/.bash_aliases
fi

# Enable bash-completion.
if [ -f "/etc/bash_completion" ]; then
  source /etc/bash_completion
fi

# Define the CPU architecture we should build for
ARCHFLAGS='-arch x86_64'
export ARCHFLAGS

# Set up default GPG key
export GPGKEY=154E77B4

# Set Ruby HEAP options
#export RUBY_HEAP_MIN_SLOTS=1000000
#export RUBY_HEAP_SLOTS_INCREMENT=1000000
#export RUBY_HEAP_SLOTS_GROWTH_FACTOR=1
#export RUBY_GC_MALLOC_LIMIT=1000000000
#export RUBY_HEAP_FREE_MIN=500000
#export RUBY_FREE_MIN=500000

# This is purely for the non-updated apps that need it.
GOROOT="/usr/local/go"
GOPATH="$GOROOT/bin"
export GOROOT GOPATH

# Set up Java
#JAVA_HOME="/usr/local/java/jdk1.8.0_25"
#PATH="/usr/local/java/jdk1.8.0_25/bin:$PATH"
#export PATH

# Added by the Heroku Toolbelt
export PATH="/usr/local/heroku/bin:$PATH"

# Export RSPEC and AUTOFEATURE=true for autotest to run cucumber features, and rspec tests
export RSPEC=true
export AUTOFEATURE=TRUE

## Ensure jruby runs in 1.9 mode
#export JRUBY_OPTS="--1.9"

# rubygems-bundler support
export USE_BUNDLER=force
export BUNDLER_BLACKLIST="cheat heroku gist"

SSH_ENV="$HOME/.ssh/environment"
function start_agent {
  echo "Initialising new SSH agent..."
    /usr/bin/ssh-agent | sed 's/^echo/#echo/' > "${SSH_ENV}"
    echo succeeded
    chmod 600 "${SSH_ENV}"
    . "${SSH_ENV}" > /dev/null
    /usr/bin/ssh-add;
}

# Source SSH settings, if applicable

if [ -f "${SSH_ENV}" ]; then
  . "${SSH_ENV}" > /dev/null
#ps ${SSH_AGENT_PID} doesn't work under cywgin
  ps -ef | grep ${SSH_AGENT_PID} | grep ssh-agent$ > /dev/null || {
  start_agent;
}
else
  start_agent;
fi

# Set up RVM

# Ensure RVM is in PATH
PATH=$HOME/.rvm/bin:$PATH # Add RVM to PATH for scripting

# Set up and enable RVM's PS1 shell functions
[[ -s "$HOME/.rvm/contrib/ps1_functions" ]] && source "$HOME/.rvm/contrib/ps1_functions"

# Enable RVM tab completion
[[ -r $rvm_path/scripts/completion ]] && source $rvm_path/scripts/completion

# Set up PS1 prompt using RVM's ps1_functions
ps1_set
ps1_set --prompt ∵

# Enable tmuxinator
[[ -s $HOME/.tmuxinator/scripts/tmuxinator ]] && source $HOME/.tmuxinator/scripts/tmuxinator

# And finally, completely enable RVM for Ruby management
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
