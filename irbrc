#!/usr/bin/env ruby
$: << '.' << './lib'
require 'rbconfig'
require 'irb/completion'
require 'irb/ext/save-history'

IRB.conf[:IRB_NAME]="irb"
IRB.conf[:MATH_MODE]=false
IRB.conf[:INSPECT_MODE]=true
IRB.conf[:IRB_RC] = nil
IRB.conf[:BACK_TRACE_LIMIT]=16
IRB.conf[:USE_LOADER] = false
IRB.conf[:USE_READLINE] = true
IRB.conf[:USE_TRACER] = false
IRB.conf[:IGNORE_SIGINT] = true
IRB.conf[:IGNORE_EOF] = false
IRB.conf[:PROMPT_MODE] = :DEFAULT
IRB.conf[:DEBUG_LEVEL]=0
IRB.conf[:VERBOSE]=false
IRB.conf[:SAVE_HISTORY] = 1000
IRB.conf[:HISTORY_FILE] = "#{ENV['HOME']}/.irb_history"
IRB.conf[:AUTO_INDENT] = true

ActiveRecord::Base.logger.level = 1 if defined? ActiveRecord::Base

