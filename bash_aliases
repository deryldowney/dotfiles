alias be='bundle exec'
alias ber='bundle exec rspec'
alias l='ls -CF'
alias la='ls -AF'
alias ll='ls -lF'
alias lal='ls -alF'
alias ls='ls -GF --color=auto'
alias grep='grep --color'
alias gf='git flow'
alias irb=pry
alias rspec='rspec -c'
alias pryr="pry -r ./config/environment -r rails/console/app -r rails/console/helpers"
alias godtf='cd $HOME/projects/github/collaborations/dtf-gems/dtf'
alias gorvm='cd $HOME/projects/github/collaborations/wayneeseguin/rvm'
alias gorvm-test='cd $HOME/projects/github/collaborations/wayneeseguin/rvm-test'
alias gomyrails='cd $HOME/projects/github/deryldoucette/my_rails_website'
alias runtests="RAILS_ENV='development' bundle exec rspec spec --format doc"
alias gokoans="cd $HOME/projects/github/edgecase/ruby_koansi && rake"
alias rii='ri -i'
alias listgemset="GEM_PATH=$GEM_DIR gem list"
alias tmux="tmux -l -u"
alias dbreset="rake db:drop:all && rake db:create:all && rake db:migrate && rake db:setup && rake db:test:prepare"
