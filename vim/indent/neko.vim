" Vim indent file
" Language:	Neko
" Maintainer: Michael Fellinger <m.fellinger@gmail.com>
" URL: http://nowhere.to/be/found
" Last Change: 2007 July 12

" Only load this indent file when no other was loaded.
if exists("b:did_indent")
   finish
endif
let b:did_indent = 1

setlocal ai nosi

let b:undo_indent = "setl ai< si<"
