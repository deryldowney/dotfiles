" Vim syntax file
" Language:	oxid
" Maintainer:	Michael Fellinger <m.fellinger@gmail.com>
" URL:		http://manveru.mine.nu/darcs/prog/projects/oxid-vim
" Version:	1
" Last Change:  2007 July 15
" Remark:	None, as of yet

if exists("b:current_syntax")
  finish
endif

syn cluster oxidAny
  \ contains=oxidString

syn region oxidMethod
  \ start=/function(\.\*){/
  \ end=/}/

syn region nekoString
  \ matchgroup=nekoString
  \ start=/"/ skip=/\\./ end=/"/
  \ contains=@Spell

syn region nekoString
  \ matchgroup=nekoString
  \ start=/'/ skip=/\\./ end=/'/
  \ contains=@Spell

syn region nekoUnit matchgroup=nekoUnitDelims start=/(/ end=/)/ transparent
syn region nekoUnit matchgroup=nekoUnitDelims start=/\[/ end=/\]/ transparent
syn match nekoError /)/
syn match nekoError /\]/

syn match nekoOperator display /\<[*+/%=><-]\+\>/
syn match nekoOperator display /\<\(==\|<=\|=>\|>=\)\>/
syn cluster nekoAny add=nekoOperator
syn region nekoComment
  \ start=/\/\*/ end=/\*\//
  \ contains=nekoTodo
syn cluster nekoAny add=nekoComment
syn region nekoComment
  \ start=/#/ end=/$/
  \ contains=nekoTodo
syn match nekoInteger display /\<\d\+\>/
syn cluster nekoAny add=nekoInteger
syn match nekoFloat   display /\<\d\+.\d\+>/
syn cluster nekoAny add=nekoFloat
syn region nekoPreProc
  \ start=/function\<(/ end=/\>)/
  \ contains=nekoAny
syn cluster nekoAny add=nekoPreProc
" 
syn keyword nekoBoolean true false nil
syn match nekoSpecial display /\(\w\|-\)\+:/
 
hi def link nekoBoolean     Boolean
hi def link nekoCharacter   Character
hi def link nekoComment     Comment
hi def link nekoConditional Conditional
hi def link nekoConstant    Constant
hi def link nekoDelimiter   Delimiter
hi def link nekoError       Error
hi def link nekoFloat       Float
hi def link nekoFunction    Function
hi def link nekoIdentifier  Identifier
hi def link nekoKeyword     Keyword
hi def link nekoNumber      Number
hi def link nekoOperator    Operator
hi def link nekoPreProc     PreProc
hi def link nekoSpecial     Special
hi def link nekoSpecialChar SpecialChar
hi def link nekoStatement   Statement
hi def link nekoString      String
hi def link nekoTodo        Todo

" syn match stError /\S/ contained
" syn match stBangError /!/ contained
" 
" syn match stDelimError /)/
" syn match stDelimError /\]/
" syn match stDelimError /}/
" 
" syn match nekoCharacter display /\<\w\+\>/
" syn region nekoString
"     \ start=/"/ end=/"/
"     \ contains=@Spell
" syn region nekoFunction
"     \ start=/\w+(/ end=/)/

" " Vim syntax file
" " Language:     Smalltalk
" " Maintainer:   Jānis Rūcis <parasti@gmail.com>
" " Last Change:  2007-03-24
" " Remark:       Probably contains a lot of GNU Smalltalk-specific stuff.
" 
" if exists("b:current_syntax")
"     finish
" endif
" 
" let s:cpo_save = &cpo
" set cpo&vim
" 
" " -----------------------------------------------------------------------------
" 
" " Letters and digits only.
" setlocal iskeyword=a-z,A-Z,48-57
" 
" " Still not perfect.
" syn sync minlines=300
" 
" syn case match
" 
" " -----------------------------------------------------------------------------
" 
" syn match stError /\S/ contained
" syn match stBangError /!/ contained
" 
" syn match stDelimError /)/
" syn match stDelimError /\]/
" syn match stDelimError /}/
" 
" " stMethodMembers holds groups that can appear in a method.
" syn cluster stMethodMembers contains=stDelimError
" 
" " -----------------------------------------------------------------------------
" 
" syn keyword stTodo FIXME TODO XXX contained
" syn region stComment
"     \ start=/"/ end=/"/
"     \ contains=stTodo,@Spell
"     \ fold
" 
" syn cluster stMethodMembers add=stComment
" 
" " -----------------------------------------------------------------------------
" 
" syn keyword stNil nil
" syn keyword stBoolean true false
" syn keyword stKeyword self super
" 
" syn cluster stMethodMembers add=stNil,stBoolean,stKeyword
" 
" " -----------------------------------------------------------------------------
" 
" syn region stMethods
"     \ matchgroup=stMethodDelims
"     \ start=/!\%(\K\k*\.\)*\K\k*\s\+\%(class\s\+\)\?methodsFor:[^!]\+!/
"     \ end=/\_s\@<=!/
"     \ contains=stError,stMethod,stComment
"     \ transparent fold
" 
" " -----------------------------------------------------------------------------
" 
" " Unary messages
" syn region stMethod
"     \ matchgroup=stMessagePattern
"     \ start=/\K\k*\%(\_s\+\)\@=/
"     \ end=/!/
"     \ contains=@stMethodMembers
"     \ contained transparent fold
" 
" " Binary messages
" syn region stMethod
"     \ matchgroup=stMessagePattern
"     \ start=/[-+*/~|,<>=&@?\\%]\{1,2}\s\+\K\k*\%(\_s\+\)\@=/
"     \ end=/!/
"     \ contains=@stMethodMembers
"     \ contained transparent fold
" 
" " Keyword messages
" syn region stMethod
"     \ matchgroup=stMessagePattern
"     \ start=/\%(\K\k*:\s\+\K\k*\_s\+\)*\K\k*:\s\+\K\k*\%(\_s\+\)\@=/
"     \ end=/!/
"     \ contains=@stMethodMembers
"     \ contained transparent fold
" 
" " -----------------------------------------------------------------------------
" 
" " Format spec, yeah right.  :)
" syn match stFormatSpec /%\d/ contained
" 
" syn match stSpecialChar /''/ contained
" 
" syn region stString
"     \ matchgroup=stString
"     \ start=/'/ skip=/''/ end=/'/
"     \ contains=stSpecialChar,stFormatSpec,@Spell
" 
" syn match stCharacter /$./
" 
" " stLiterals holds all, uh, literals.
" syn cluster stLiterals contains=stString,stCharacter
" syn cluster stMethodMembers add=stString,stCharacter
" 
" " -----------------------------------------------------------------------------
" 
" syn region stHashedString
"     \ matchgroup=stHashedString
"     \ start=/#'/ skip=/''/ end=/'/
"     \ contains=stSpecialChar
" 
" syn match stQuotedSelector /#\K\k*/ display
" " Standard draft says [!%&*+,/<=>?@\\~|-].
" syn match stQuotedSelector /#[-+*/~|,<>=&@?\\%]\+/ display
" syn match stQuotedSelector /#\%(\K\k*:\)\+/ display
" 
" syn cluster stSymbol contains=stHashedString,stQuotedSelector
" 
" syn cluster stLiterals add=@stSymbol
" syn cluster stMethodMembers add=@stSymbol
" 
" " -----------------------------------------------------------------------------
" 
" " Mainly for highlighting mismatched parentheses
" syn region stUnit matchgroup=stUnitDelims start=/(/ end=/)/ transparent
" 
" syn cluster stMethodMembers add=stUnit
" 
" " -----------------------------------------------------------------------------
" 
" " Its ugly look is entirely stEval's fault.
" syn match stArrayConst /\%(#\[\|#\@<!#(\)/me=e-1 nextgroup=stArray,stByteArray
" 
" syn region stArray
"     \ matchgroup=stArrayDelims
"     \ start=/(/ end=/)/
"     \ contains=@stLiterals,stComment,stArray,stByteArray,stNil,stBoolean
"     \ contained transparent fold
" syn region stByteArray
"     \ matchgroup=stByteArrayDelims
"     \ start=/\[/ end=/\]/
"     \ contains=@stNumber,stComment
"     \ contained transparent fold
" 
" syn cluster stLiterals add=stArrayConst
" syn cluster stMethodMembers add=stArrayConst
" 
" " -----------------------------------------------------------------------------
" 
" syn match stBindingDelims /[{}]/ contained
" syn match stBinding /#{\s*\%(\K\k*\.\)*\K\k*\s*}/ contains=stBindingDelims
" 
" syn region stEval
"     \ matchgroup=stEvalDelims
"     \ start=/##(/ end=/)/
"     \ contains=@stMethodMembers
"     \ transparent
" 
" syn cluster stLiterals add=stBinding,stEval
" syn cluster stMethodMembers add=stBinding,stEval
" 
" " -----------------------------------------------------------------------------
" 
" syn match stInteger display       /\<\d\+\>/
" syn match stRadixInteger display  /\<\d\+r[0-9A-Z]\+\>/
" syn match stFloat display         /\<\d\+\.\d\+\%([edq]-\?\d\+\)\?\>/
" syn match stScaledDecimal display /\<\d\+\%(\.\d\+\)\?s\%(\d\+\)\?\>/
" 
" " syn match stNumber
" "     \ /-\?\<\d\+\%(\.\d\+\)\?\%([deqs]\%(-\?\d\+\)\?\)\?\>/
" "     \ display
" " syn match stNumber
" "     \ /\<\d\+r-\?[0-9A-Z]\+\%(\.[0-9A-Z]\+\)\?\%([deqs]\%(-\?\d\+\)\?\)\?\>/
" "     \ display
" 
" syn cluster stNumber contains=st\%(Radix\)\?Integer,stFloat,stScaledDecimal
" 
" syn cluster stLiterals add=@stNumber
" syn cluster stMethodMembers add=@stNumber
" 
" " -----------------------------------------------------------------------------
" 
" " Pretty-printing for various groups.
" syn match stDelimiter /|/ contained display
" syn match stIdentifier /\K\k*/ contained display
" 
" " -----------------------------------------------------------------------------
" 
" " To FIXME or not to FIXME?  I will match at "boolean | boolean | boolean".
" syn region stTemps
"     \ matchgroup=stTempDelims
"     \ start=/|/ end=/|/
"     \ contains=stIdentifier,stError
" 
" syn cluster stMethodMembers add=stTemps
" 
" " -----------------------------------------------------------------------------
" 
" " Blocks
" 
" " I made up the name.
" syn match stBlockConditional /\<whileTrue\>:\?/ contained
" syn match stBlockConditional /\<whileFalse\>:\?/ contained
" 
" syn match stBlockTemps
"     \ /\[\@<=\_s*\%(:\K\k*\s\+\)*:\K\k*\s*|/
"     \ contains=stIdentifier,stDelimiter
"     \ contained transparent
" 
" syn region stBlock
"     \ matchgroup=stBlockDelims
"     \ start=/\[/ end=/\]/
"     \ contains=@stMethodMembers,stBlockTemps,stBangError
"     \ nextgroup=stBlockConditional skipempty skipwhite
"     \ transparent fold
" 
" syn cluster stMethodMembers add=stBlock
" 
" " -----------------------------------------------------------------------------
" 
" syn match stAssign /:=/
" syn match stAnswer /\^/
" 
" syn cluster stMethodMembers add=stAssign,stAnswer
" 
" " -----------------------------------------------------------------------------
" 
" syn region stCollect
"     \ matchgroup=stCollectDelims
"     \ start=/{/ end=/}/
"     \ contains=@stLiterals,stCollect,stBlock,stNil,stKeyword,stBoolean,stAssign,stComment,stBangError
"     \ transparent fold
" 
" syn cluster stMethodMembers add=stCollect
" 
" " -----------------------------------------------------------------------------
" 
" syn match stConditional /\<ifTrue:/
" syn match stConditional /\<ifFalse:/
" syn match stConditional /\<and:/
" syn match stConditional /\<eqv:/
" syn match stConditional /\<or:/
" syn match stConditional /\<xor:/
" syn match stConditional /\<not\>/
" 
" syn cluster stMethodMembers add=stConditional
" 
" " -----------------------------------------------------------------------------
" 


" 
" " -----------------------------------------------------------------------------
" 
" let b:current_syntax = "st"
" 
" let &cpo = s:cpo_save
" unlet s:cpo_save
