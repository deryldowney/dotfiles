" Vim syntax file
" Language:		nagoro
" Maintainer:		Tim Pope <vimNOSPAM@tpope.info>
" Info:			$Id: nagoro.vim,v 1.18 2007/05/06 23:56:12 tpope Exp $
" URL:			http://vim-ruby.rubyforge.org
" Anon CVS:		See above site
" Release Coordinator:	Doug Kearns <dougkearns@gmail.com>

if exists("b:current_syntax")
  finish
endif

if !exists("main_syntax")
  let main_syntax = 'nagoro'
endif

if !exists("g:nagoro_default_subtype")
  let g:nagoro_default_subtype = "html"
endif

if !exists("b:nagoro_subtype") && main_syntax == 'nagoro'
  let s:lines = getline(1)."\n".getline(2)."\n".getline(3)."\n".getline(4)."\n".getline(5)."\n".getline("$")
  let b:nagoro_subtype = matchstr(s:lines,'nagoro_subtype=\zs\w\+')
  if b:nagoro_subtype == ''
    let b:nagoro_subtype = matchstr(substitute(expand("%:t"),'\c\%(\.erb\)\+$','',''),'\.\zs\w\+$')
  endif
  if b:nagoro_subtype == 'xhtml'
    let b:nagoro_subtype = 'html'
  elseif b:nagoro_subtype == 'zmr'
    let b:nagoro_subtype = 'html'
  elseif b:nagoro_subtype == 'rb'
    let b:nagoro_subtype = 'ruby'
  elseif b:nagoro_subtype == 'yml'
    let b:nagoro_subtype = 'yaml'
  elseif b:nagoro_subtype == 'js'
    let b:nagoro_subtype = 'javascript'
  elseif b:nagoro_subtype == 'txt'
    " Conventional; not a real file type
    let b:nagoro_subtype = 'text'
  elseif b:nagoro_subtype == ''
    let b:nagoro_subtype = g:nagoro_default_subtype
  endif
endif

if !exists("b:nagoro_nest_level")
  let b:nagoro_nest_level = strlen(substitute(substitute(substitute(expand("%:t"),'@','','g'),'\c\.\%(erb\|rhtml\)\>','@','g'),'[^@]','','g'))
endif
if !b:nagoro_nest_level
  let b:nagoro_nest_level = 1
endif

if exists("b:nagoro_subtype") && b:nagoro_subtype != ''
  exe "runtime! syntax/".b:nagoro_subtype.".vim"
  unlet! b:current_syntax
endif
syn include @rubyTop syntax/ruby.vim

syn cluster nagoroRegions contains=nagoroOneLiner,nagoroBlock,nagoroExpression,nagoroComment

exe 'syn region nagoroBlock matchgroup=nagoroDelimiter      start="<?r\{1,'.b:nagoro_nest_level.'\}%\@!-\=" end="-\=?>" contains=@rubyTop containedin=ALLBUT,@erbRegions'
exe 'syn region nagoroExpression matchgroup=nagoroDelimiter start="#{\{1,'.b:nagoro_nest_level.'\}%\@!-\=" end="-\=}" contains=@rubyTop containedin=ALLBUT,@erbRegions'

" Define the default highlighting.
" For version 5.7 and earlier: only when not done already
" For version 5.8 and later: only when an item doesn't have highlighting yet
if version >= 508 || !exists("did_nagoro_syntax_inits")
  if version < 508
    let did_ruby_syntax_inits = 1
    command -nargs=+ HiLink hi link <args>
  else
    command -nargs=+ HiLink hi def link <args>
  endif

  HiLink nagoroDelimiter		Delimiter
  HiLink nagoroComment		Comment

  delcommand HiLink
endif
let b:current_syntax = 'nagoro'

if main_syntax == 'nagoro'
  unlet main_syntax
endif

" vim: nowrap sw=2 sts=2 ts=8 ff=unix:
