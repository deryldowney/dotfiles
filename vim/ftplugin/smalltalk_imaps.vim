let maplocalleader=";"

"=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
" Class
"=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
inoremap <buffer><localleader>sub <c-r>=IMAP_PutTextWithMovement("<+ class +> subclass: #<+ new class +>\ninstanceVariableNames: '<+ instance variables +>'\nclassVariableNames: '<+ class variables +>'\npoolDictionaries: '<+ pool dictionaries +>'\ncategory: <+ category +> !")<cr>
